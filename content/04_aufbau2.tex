\chapter{Experimentelle Details} \label{chapter:exp_details}

Im Folgenden sollen alle für das experimentelle Gewinnen von Daten notwendigen Aufbauten und Methoden vorgestellt werden. Dies beinhaltet NMR-Aufbauten wie das Spektrometer und die Probenköpfe, die Weiterverarbeitung der Daten, sowie die untersuchten Proben und deren Herstellung.

\section{Spektrometer} \label{section:exp:spektrometer}

Es soll der Aufbau der Spektrometer, der in Abbildung \ref{fig:exp:aufbau} zu sehen ist, vorgestellt werden. Der exakte Aufbau unterscheidet sich je nach Spektrometer; während hier der allgemeine Aufbau beschrieben werden soll, beziehen sich die beispielhaft genannten Geräte auf das Spektrometer OHI ***. Bei nicht weiter spezifizierten Geräten handelt es sich um Eigenbauten.

Die Weiterverarbeitung der mit dem Spektrometer gewonnenen Daten, sowie die Temperaturkontrolle (die, ebenso wie der supraleitende Magnet in der Abbildung nicht gezeigt ist), werden danach besprochen.

\begin{figure}
	\begin{center}
		\includegraphics[width=\textwidth]{graphics/joachim/aufbau.pdf}
	\end{center}
	\caption{Skizzierter Aufbau eines NMR-Spektrometers. Abbildung aus *** \cite[S. 29]{lueg_implementierung_2016}} \label{fig:exp:aufbau}
\end{figure}

Die Beschreibung versucht den Aufbau entlang des Signalwegs nachzuvollziehen. Die Steuerung aller Messungen geschehen über einen Computer, der mit der in Python geschriebenen Software DAMARIS (Darmstadt Magnetic Resonance Instrument Software \cite{gadke_damaris_2007}) ausgestattet ist. Diese erlaubt es, Messprogramme zu definieren, mit Hilfe von Pythons \emph{serial}-Schnittstelle die Verbindung zu verschiedenen Geräten herzustellen, und die produzierten Signale aufzunehmen, anzuzeigen und zur Weiterverarbeitung zu speichern.

Durchgängig läuft ein Frequenzgenerator vom Typ PTS 310, dessen Frequenz auf die Lamorfrequenz des zu untersuchenden Kerns im jeweiligen Magneten eingestellt ist. Ist diese Frequenz nicht bekannt, empfiehlt sich in der Regel das Aufnehmen eines Spektrums von einer Flüssigkeit, die den gewünschten Kern enthält. Aufgrund der geringen Breite des Flüssigkeitsspektrum lässt sich so die Lamorfrequenz bestimmen.

Soll ein Puls an den Probenkopf gesandt werden, setzt DAMARIS über eine 24-bit-Puls-Karte für die Dauer des Pulses ein Bit auf 1, was mit Hilfe eines HF-Schalters aus dem kontinuierlichen Signal des Frequenzgenerators einen Puls mit der gewünschten Länge ausschneidet. Die ist in Abbildung (***) zu sehen.

Dieser Puls wird nun gedämpft. Über diese variable Dämpfung lässt sich die Stärke des Pulses und damit nach Formel $B \propto I$ *** das in der Probenspule entstehende Magnetfeld variieren. Dieses wiederum ist nach Formel $\phi = - \gamma B_1 t_\text{Puls}$ *** entscheidend für die Länge aller Pulse. Damit die hier angenommene Näherung, dass die während der Pulse stattfindende Dynamik vernachlässigbar ist, sollte die Dämpfung so eingestellt werden, dass sich die Länge eines Invertierungspulses im Bereich weniger Mikrosekunden befindet.

Der so gedämpfte Puls wird dann mit einem dressler Verstärker mit $\SI{2}{\kilo\watt}$ Leistung um einen konstanten Faktor verstärkt und trifft dann über einen Richtkoppler auf den Probenkopf. Der Richtkoppler verhindert dabei, dass der leistungsstarke Puls auf die Detektionselektronik trifft, die für die verstärkung schwacher Signale ausgelegt ist und entsprechend empfindlich ist.

Das nach dem Puls vom Probenkopf zurückgegebene Signal wird vom Richtkoppler zur Signal-Detektion geleitet. Es wird zunächst von einem Vorverstärker verstärkt, ehe es gesplittet wird. Beide Teile werden mit dem vom Frequenzgenerator erzeugten Sinus-Schwingung, welches die Lamorfrequenz hat, gemischt; eine der Schwingungen ist jedoch um $\SI{90}{^\circ}$ phasenverschoben. Die so entstehenden Signale werden Real- und Imaginärteil des Signals genannt. Zusätzlich können beide Teile um eine konstante Phase verschoben werden -- diese lässt sich wiederum über DAMARIS mit der Phasenansteuerungs-Karte einstellen.

Im nachfolgenden Tiefpass werden alle hochfrequenten Teile des Signals abgeschnitten, sodass nur die durch das Mischen entstandende Differenz $\Delta \omega = \omega_L - \omega_\text{Signal}$ verbleibt. Es lässt sich die Cutoff-Frequenz, sowie die Amplitude und der Offset beider Einzelsignale einstellen. Die letzteren beiden sollten so gewählt sein, dass der Offset Null ist und beide Signale die gleiche Amplitude haben.

Beide Signale werden mit einem Nachverstärker verstärkt, ehe sie von einer ADC-Karte (engl. ADC: Analog-Digital-Konverter) des Computers in Form von Amplituden-Werten in einer gesetzten Frequenz von bis zu $\SI{10}{MHz}$ aufgenommen werden. Die ADC-Karte wird dabei über die Puls-Karte mit einem Trigger geschaltet. Dies kann notwendig sein, da vor dem eigentlichen Signal noch unerwünschte Effekte, wie zum Beispiel ein Nachschwingen der Probenspule, auftreten können. Um diese abzuschneiden, kann das Aufnehmen des Signals um eine festgelegte Trigger-Zeit verzögert werden.


\section{Weiterverarbeitung der experimentellen Daten} \label{section:exp:weiterverarbeitung}

Alle so aufgenommen Daten bestehen also aus einem Zeitpunkt $t$ und dem zugehörigen Amplitudenwert von Real- und Imaginärteil. Werden Messungen wie eine $T_1$-, $T_2$- oder $F_2$-Messung durchgeführt, ergeben sich variablenabhängige Messreihen. Es wird diese Variable (z.B. Evolutions- oder Mischzeit) meist logarithmisch gegen das Maximum des Realteils der jeweiligen Messung aufgetragen; dann können die Daten analysiert werden. Um ein möglichst großes Realteil-Signal am Ort $t_\text{Max}$ des Maximums zu gewährleisten, sollte dort der Imaginärteil einen Nulldurchgang aufweisen. Dies lässt sich mit einer entsprechenden Einstellung der Phase über die Phasenansteuerungs-Karte erreichen.

Sollen Spektren untersucht werden, wird das Zeitsignal fouriertranformiert. Für eine hohe Auflösen sollte ein möglichst langes Zeitsignal aufgenommen werden; für eine große Spanne, ein Zeitsignal mit möglichst hoher Frequenz der ADC-Karte.



\section{Probenköpfe} \label{section:exp:probenkoepfe}

NMR-Messungen bei mehreren Hundert Grad Celsius durchzuführen kann sich mitunter als eine Herausforderung gestalten. Lötzinne, die häufig verwendet werden um die Resonanzspule an dem Probenkopf anzubringen, schmelzen je nach Art bei weniger $\SI{200}{\degreeCelsius}$. Aber auch Tuningkondensator und Matchingspule sowie der für die Resonanzspule verwendete Draht sind häufig nur für bestimmte Temperaturen ausgelegt. Zudem ist es offensichtlich, dass für höhere Temperaturen eine gute Isolierung und gegebenenfalls Kühlung des Probenkopfs gegeben sein muss, ist er doch in direkter Umgebung des mit flüssigem Helium gekühlten Magneten.

Für solche Anwendungen ist also ein spezieller Probenkopf notwendig, welcher in Form des Hochtemperaturprobenkopfs (intern als „Ofen“ bezeichnet) als eine Spezialanfertigung realisiert wurde, dessen Konstruktionszeichnung in Abbildung \ref{fig:exp:ofen_aufbau} zu sehen.

\begin{sidewaysfigure}
	\begin{figure}[H]
		\includegraphics[width=1.05\textheight]{graphics/ofen/ofen_aufbau2.pdf}
		\caption{Konstruktionszeichnung des Hochtemperaturprobenkopfes \cite{Rudloff_blue_print}.}
		\label{fig:exp:ofen_aufbau}
	\end{figure}
\end{sidewaysfigure}

Er lässt sich grob in drei Teilen beschreiben: Im oberen Drittel befindet sich der Schwingkreis mit der Probe, eine Vorrichtung, um diese zu erwärmen, sowie die dazugehörige notwendige thermische Isolation. Am unteren Drittel werden alle notwendigen Verbindungen angeschlossen; das mittlere Drittel verbindet beides und beherbergt einen Netzfilter.

Um Temperaturen von bis zu $\SI{1100}{K}$ erreichen zu können, wird ein Heizdraht aus einer FeCrAl-Legierung verwendet. Dieser ist bifilar über ein Hartkeramik-Hohlzylinder gewickelt, um möglichst wenig störende Magnetfelder entstehen zu lassen -- gerade in direkter Umgebung der Probe. Diese Vorrichtung ist im Deckel untergebracht und lässt sich so über die aus Platin bestehende Probenspule stülpen.

Um die entstehende Hitze möglichst gut nach außen hin zu isolieren, ist dieser Aufbau von poröser Keramik umgeben -- sowohl innerhalb des Deckels als auch zwischen Probenspule und Rest des Probenkopfes. Bei letzterem ist zudem eine weitere Scheibe aus dichter Keramik vorhanden. Für die Probenspule als auch Temperaturfühler sind Bohrungen in den Keramikscheiben vorhanden.

Darunter befindet sich der Schwingkreis der Probenspule. Sowohl eine Matchingspule, als auch ein variabler Kondensator und ein Halterung für einen wechselbaren Kondensator sind vorhanden. Es lassen sich Kondensatoren mit verschiedenen Kapazitäten in der Halterung anbringen; zusammen mit dem variablen Kondensator kann so für die Resonanzfrequenz eine Spanne von $\SI{63,6}{MHz}$ bis $\SI{168,9}{MHz}$ (mit Unterbrechungen) abgedeckt werden.

% Darunter befindet sich die Möglichkeit für eine Wasserkühlung, ***

Der sich in dem Verbindungsteil befindliche Netzfilter soll den Heizstrom von störenden Anteilen befreien. Dazu wird eine Spule mit eisernem Kern verwendet. Während diese Drossel den Probenkopf in der Vergangenheit genau im Hauptfeld des Magneten gehalten hatte, ist dies jetzt nicht mehr der Fall. Da zudem ungewünschte Einflüsse durch das zusätzliche Magnetfeld zu befürchten sind, liegt die Idee nahe, den vorliegenden Aufbau zu ändern. Eine mögliche Anpassung könnte sein, den Netzfilter außerhalb des Probenkopfes anzubringen und die Positionierung des Probenkopfes im Hauptfeld des Magneten durch eine zusätzliche Halterung zu gewährleisten.

Um den Aufbau zu betreiben sind im letzten Drittel eine Reihe von Anschlüssen vorhanden:
\begin{itemize}
	\item Ein BNC-Anschluss der vom Richtkoppler kommend zum Schwingkreis führt,
	\item Bohrungen für die beiden Thermoelemente, welche von einem Temperatur-Controller ausgelesen werden,
	\item ein Anschluss für den Heizstrom, welcher von einem Temperatur-Controller bereitgestellt wird,
	\item je eine Rändelstange für die Einstellung von Matchingspule und Tuningkondensator,
	\item Anschlüsse für Zu- und Abflussschläuche für das Kühlwasser. Der Zufluss kann mit einem Wasserhahn gespeist werden.
\end{itemize}

„Reguläre“ Probenköpfe sind im Vergleich recht einfach gehalten. Da die Temperatursteuerung meist durch den Kryostaten geleistet wird, fallen die Heizeinheit, Isolation, Wasserkühlung und Netzfilter heraus und es bleiben nur Schwingkreis und Temperaturfühler. In dem hier verwendeten Probenkopf P1 ist zudem keine Matchingspule vorhanden, sodass sich das Abstimmen des Probenkopfs deutlich umständlicher gestaltet.




\section{Temperaturkontrolle} \label{section:exp:temperaturkontrolle}

Um häufig gewünschte temperaturabhängige Messungen durchführen zu können, lassen sich über Python-Skripte Temperatur-Controller, wie zum Beispiel die hier verwendeten Lakeshore-Einheiten, benutzen. So wird ein Auslesen der aktuellen Temperatur sowie das Setzen einer Soll-Temperatur ermöglicht. Der Temperatur-Controller wiederum steuert Temperatureinheiten im Probenkopf (im Falle des Hochtemperaturprobenkopfs) oder im Kryostaten (im Falle des Probenkopfs P1) an. Die aktuelle Temperatur wird über Temperaturfühler erfasst. Dabei handelt es sich im Falle des Hochtemperaturprobenkopfes um Thermoelemente vom Typ N, im Falle des Probenkopfes P1 um PT100-Elemente ***.





\section{Proben} \label{section:exp:proben}

Bei den durchgeführten Messungen wurden drei verschiedene Proben verwendet:

Tabelle ***

Probe 1*** stammt von C. Zürn; die Herstellung der Probe ist in \cite{zuern_arbeit} dokumentiert.

Die Proben 2 und 3 wurden für diese Arbeit angefertigt. Dafür wurde CRN (was genau für welches? ***) zu einem möglichst feinen Pulver zermörsert und ein gerades bzw. gebogenes Duran-Glas-Röhrchen gefüllt. Umstörende Einflüsse von Feuchtigkeit in der Probe möglichst gut zu elimieren, wurden die Proben über Nacht bei $\SI{160}{\degreeCelsius}$ in einem evakuierten Ofen getrocknet, der im Anschluss mit Stickstoffgas gelöscht wurde.

So präpariert wurden die Proben unter Anwendung eines Freeze-Pump-Thaw-Verfahrens von Herrn Kollman der physikalischen Glasbläserei der TU Dortmund abgeschmolzen. Dabei soll der Probe möglichst viel Gas entzogen werden, indem sie Vakuum ausgesetzt wird. Damit die Probe aber nicht mit angesaugt wird, wird sie vor dem Abpumpen eingefroren und danach wieder getaut.  Anschließend wurden die Proben durch erhitzen geschmolzen um sie dann in einem Glaszustand abkühlen zu lassen.

Mit dem gleichen Verfahren wurde am 13.07.2017 eine weiter Probe erstellt, wobei hier die Probe \emph{vor} dem Abschmelzen in Glaszustand gebracht wurde. Diese Probe wurde jedoch nicht verwendet, da Tests zeigten, dass sie kaum stabil im Glaszustand zu halten war sondern schnell Risse aufwies oder auskristallisierte.
