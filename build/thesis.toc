\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {german}
\contentsline {chapter}{\numberline {1}Einleitung}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Theoretische Einführung}{2}{chapter.2}
\contentsline {chapter}{\numberline {3}Bewegungsmodelle}{3}{chapter.3}
\contentsline {chapter}{\numberline {4}Experimentelle Details}{4}{chapter.4}
\contentsline {section}{\numberline {4.1}Spektrometer}{4}{section.4.1}
\contentsline {section}{\numberline {4.2}Weiterverarbeitung der Signale}{6}{section.4.2}
\contentsline {section}{\numberline {4.3}Probenköpfe}{6}{section.4.3}
\contentsline {section}{\numberline {4.4}Temperaturkontrolle}{9}{section.4.4}
\contentsline {section}{\numberline {4.5}Proben}{10}{section.4.5}
\contentsline {chapter}{\numberline {5}Experimentelle Resultate und Analysen}{12}{chapter.5}
\contentsline {chapter}{\numberline {6}Zusammenfassung und Ausblick}{13}{chapter.6}
\contentsline {chapter}{\nonumberline Literatur}{14}{chapter*.7}
